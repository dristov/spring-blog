# Spring Blog App #

This repo presents a simple blog app made with Spring Boot.
It uses the [Thymeleaf](http://www.thymeleaf.org/) template engine for rendering views.
It also contains a JSON API for reading posts.

It provides the following functionalities:

- User login/registration
- User roles (admin, mod, user)
- List posts with pagination
- Find posts by category and author
- Search posts
- Comments
- Markdown support