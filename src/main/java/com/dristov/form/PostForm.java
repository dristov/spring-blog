package com.dristov.form;

import com.dristov.domain.Post;
import com.dristov.domain.PostCategory;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class PostForm {

    @NotEmpty
    private String title;

    @NotEmpty
    private String content;

    @NotNull
    private PostCategory category;

    private boolean draft;

    private Date createdAt;
    private Date publishedAt;

    public PostForm() {
    }

    public PostForm(Post post) {

        title = post.getTitle();
        content = post.getRawContent();
        category = post.getCategory();
        draft = post.isDraft();
        publishedAt =  post.getPublishedAt();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public PostCategory getCategory() {
        return category;
    }

    public void setCategory(PostCategory category) {
        this.category = category;
    }

    public boolean isDraft() {
        return draft;
    }

    public void setDraft(boolean draft) {
        this.draft = draft;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(Date publishedAt) {
        this.publishedAt = publishedAt;
    }
}
