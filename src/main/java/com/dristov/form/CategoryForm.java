package com.dristov.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CategoryForm {

    @NotNull
    @Size(min = 2, max = 50)
    private String name;

    public CategoryForm() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
