package com.dristov.service;

import com.dristov.domain.Account;

public interface AccountService {

    void save(Account account);

    Account findByUsername(String username);
}
