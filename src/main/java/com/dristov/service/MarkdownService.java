package com.dristov.service;

import org.pegdown.PegDownProcessor;
import org.springframework.stereotype.Service;

@Service
public class MarkdownService {

    private final PegDownProcessor processor = new PegDownProcessor();

    public MarkdownService() {
//        processor = new PegDownProcessor(Extensions.ALL ^ Extensions.ANCHORLINKS);
    }

    public String mdToHtml(String markdown) {

        synchronized (processor) {

            return processor.markdownToHtml(markdown);
        }
    }
}
