package com.dristov.service;

import com.dristov.domain.*;
import com.dristov.domain.exception.PostNotFoundException;
import com.dristov.form.PostForm;
import com.dristov.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class BlogService {

    private static final String CACHE_NAME = "cache.blog";

    private BlogRepository repository;
    private PostCategoryRepository categoryRepository;
    private AccountRepository accountRepository;
    private CommentRepository commentRepository;

    private MarkdownService mdService;

    @Autowired
    public BlogService(
            BlogRepository repository,
            PostCategoryRepository categoryRepository,
            AccountRepository accountRepository,
            CommentRepository commentRepository,
            MarkdownService mdService) {

        this.repository = repository;
        this.categoryRepository = categoryRepository;
        this.accountRepository = accountRepository;
        this.commentRepository = commentRepository;
        this.mdService = mdService;
    }

    public Page<Post> getPublishedPosts(Pageable pageable) {
        return repository.findByDraftFalseOrderByPublishedAtDesc(pageable);
    }

    public Page<Post> getPostsByAuthor(Account author, Pageable pageable) {
        return repository.findByDraftFalseAndAuthorOrderByPublishedAtDesc(author, pageable);
    }

    @Cacheable(CACHE_NAME)
    public Post getPostBySlug(String slug) {
        return repository.findBySlug(slug);
    }

    public Page<Post> getPostsByCategory(String categorySlug, Pageable pageable) {

        PostCategory category = categoryRepository.findByUrlSlug(categorySlug);
        return repository.findByDraftFalseAndCategoryOrderByPublishedAtDesc(category, pageable);
    }

    public Page<Post> searchPosts(String query, Pageable pageable) {
        return repository.findByDraftFalseAndTitleContainingIgnoreCaseOrderByPublishedAtDesc(query, pageable);
    }

    public Post addPost(PostForm postForm, String username) {

        Account account = accountRepository.findByUsername(username);

        Post post = new Post(
                postForm.getTitle(),
                postForm.getContent(),
                account,
                postForm.getCategory(),
                postForm.isDraft());

        post.setRenderedContent(mdService.mdToHtml(postForm.getContent()));

        if (!postForm.isDraft()) {
            post.setPublishedAt(new Date());
        }

        return repository.save(post);
    }

    @CacheEvict(value = CACHE_NAME, key = "#post.slug", condition = "#post.slug != null")
    public void deletePost(Post post) {
        repository.delete(post);
    }

    public String getCategoryName(String slug) {
        return categoryRepository.findByUrlSlug(slug).getDisplayName();
    }

    public List<PostCategory> getAllCategories() {

        List<PostCategory> categories = new ArrayList<>();
        categoryRepository.findAll().forEach(categories::add);
        return categories;
    }

    public void saveCategory(String name) {
        categoryRepository.save(new PostCategory(name));
    }

    public void addComment(String content, String user, String postSlug) {

        Post post = repository.findBySlug(postSlug);

        if (post == null) {
            throw new PostNotFoundException(postSlug);
        }

        commentRepository.save(new Comment(content, user, post));
    }

    public void deleteComment(Long commentId) {
        commentRepository.delete(commentId);
    }
}