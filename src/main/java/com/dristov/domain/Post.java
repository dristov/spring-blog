package com.dristov.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Type;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Entity
@Table(name = "posts")
public class Post {

    private static final SimpleDateFormat SLUG_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    private static final int SHORT_CONTENT_LENGTH = 150;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private Long id;

    private String title;

    @Column(nullable = false)
    @Type(type = "text")
    private String rawContent;

    @Column(nullable = false)
    @Type(type = "text")
    private String renderedContent;

    @Column(nullable = false)
    private String slug;

    @ManyToOne(cascade = CascadeType.MERGE, optional = false)
    private Account author;

    @ManyToOne
    private PostCategory category;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "post")
    private List<Comment> comments;

    private boolean draft;

    @Column(nullable = false)
    @JsonIgnore
    private Date createdAt = new Date();

    private Date publishedAt;

    public Post() {
    }

    public Post(String title, String rawContent, Account author, PostCategory category, boolean draft) {
        this.title = title;
        this.rawContent = rawContent;
        this.author = author;
        this.category = category;
        this.draft = draft;
    }

    private String generatePublicSlug() {
        return String.format("%s-%s", SLUG_DATE_FORMAT.format(getPublishedAt()), generateSlug());
    }

    private String generateSlug() {

        if (title == null) {
            return "";
        }

        String cleanedTitle = title.toLowerCase().replace("\n", " ").replaceAll("[^a-z\\d\\s]", " ");
        return StringUtils.arrayToDelimitedString(StringUtils.tokenizeToStringArray(cleanedTitle, " "), "-");
    }

    // ******************************************************************************************************
    // GETTERS & SETTERS
    // ******************************************************************************************************

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRawContent() {
        return rawContent;
    }

    public void setRawContent(String rawContent) {
        this.rawContent = rawContent;
    }

    public String getRenderedContent() {
        return renderedContent;
    }

    public void setRenderedContent(String renderedContent) {
        this.renderedContent = renderedContent;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Account getAuthor() {
        return author;
    }

    public void setAuthor(Account author) {
        this.author = author;
    }

    public PostCategory getCategory() {
        return category;
    }

    public void setCategory(PostCategory category) {
        this.category = category;
    }

    public boolean isDraft() {
        return draft;
    }

    public void setDraft(boolean draft) {
        this.draft = draft;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(Date publishedAt) {
        this.publishedAt = publishedAt;
        this.slug = publishedAt == null ? null : generatePublicSlug();
    }

    @JsonIgnore
    public String getFormattedDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.US);
        return dateFormat.format(publishedAt);
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    @JsonIgnore
    public String getAuthorName() {

        if (author.getFullName() != null) {
            return author.getFullName();
        }

        return author.getUsername();
    }

    @JsonIgnore
    public String getShortContent() {

        if (renderedContent.length() >= SHORT_CONTENT_LENGTH) {
            return renderedContent.substring(0, SHORT_CONTENT_LENGTH) + "...";
        }

        return rawContent;
    }
}
