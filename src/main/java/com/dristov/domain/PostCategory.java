package com.dristov.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "categories")
public class PostCategory {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private Long id;

    @Column(nullable = false)
    private String displayName;

    @Column(nullable = false)
    private String urlSlug;

    @OneToMany(mappedBy = "category", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<Post> posts;

    public PostCategory() {
    }

    public PostCategory(String displayName) {
        this.displayName = displayName;
        this.urlSlug = generateSlug();
    }

    public PostCategory(String displayName, String urlSlug) {
        this.displayName = displayName;
        this.urlSlug = urlSlug;
    }

    public Long getId() {
        return id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getUrlSlug() {
        return urlSlug;
    }

    public Set<Post> getPosts() {
        return posts;
    }

    private String generateSlug() {

        if (displayName == null) {
            return "";
        }

        String cleanedTitle = displayName.toLowerCase().replace("\n", " ").replaceAll("[^a-z\\d\\s]", " ");
        return StringUtils.arrayToDelimitedString(StringUtils.tokenizeToStringArray(cleanedTitle, " "), "-");
    }
}
