package com.dristov.domain.exception;

public class AuthorNotFoundException extends RuntimeException {

    public AuthorNotFoundException(String username) {
        super("Author " + username + " not found!");
    }
}
