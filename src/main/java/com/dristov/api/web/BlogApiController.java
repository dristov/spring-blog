package com.dristov.api.web;

import com.dristov.domain.Account;
import com.dristov.domain.Post;
import com.dristov.domain.exception.AuthorNotFoundException;
import com.dristov.domain.exception.PostNotFoundException;
import com.dristov.service.AccountService;
import com.dristov.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping(value = "/api/v1")
public class BlogApiController {

    private BlogService blogService;
    private AccountService accountService;

    @Autowired
    public BlogApiController(BlogService blogService, AccountService accountService) {
        this.blogService = blogService;
        this.accountService = accountService;
    }

    @RequestMapping(
            method = GET,
            value = "/posts"
    )
    public Page<Post> index(Pageable pageable) {
        return blogService.getPublishedPosts(pageable);
    }

    @RequestMapping(
            method = GET,
            value = "/posts/{slug}"
    )
    public Post showPost(@PathVariable("slug") String slug) {

        Post post = blogService.getPostBySlug(slug);

        if (post == null) {
            throw new PostNotFoundException(slug);
        }

        return post;
    }

    @RequestMapping(
            method = GET,
            value = "posts/user/{username}"
    )
    public Page<Post> userPosts(@PathVariable("username") String username, Pageable pageable) {

        Account account = accountService.findByUsername(username);

        if (account == null) {
            throw new AuthorNotFoundException(username);
        }

        return blogService.getPostsByAuthor(account, pageable);
    }

    @RequestMapping(
            method = GET,
            value = "/search"
    )
    public Page<Post> search(@RequestParam("q") String query, Pageable pageable) {
        return blogService.searchPosts(query, pageable);
    }

    @RequestMapping(
            method = GET,
            value = "/posts/category/{category}"
    )
    public Page<Post> showPostsForCategory(@PathVariable("category") String category, Pageable pageable) {
        return blogService.getPostsByCategory(category, pageable);
    }
}
