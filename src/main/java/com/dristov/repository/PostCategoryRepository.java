package com.dristov.repository;

import com.dristov.domain.PostCategory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostCategoryRepository extends CrudRepository<PostCategory, Long> {

    PostCategory findByUrlSlug(String urlSlug);
}
