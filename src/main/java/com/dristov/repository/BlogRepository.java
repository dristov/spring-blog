package com.dristov.repository;

import com.dristov.domain.Account;
import com.dristov.domain.Post;
import com.dristov.domain.PostCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface BlogRepository extends JpaRepository<Post, Long> {

    Post findBySlug(String slug);
    Page<Post> findByDraftFalseOrderByPublishedAtDesc(Pageable pageable);
    Page<Post> findByDraftFalseAndAuthorOrderByPublishedAtDesc(Account author, Pageable pageable);
    Page<Post> findByDraftFalseAndCategoryOrderByPublishedAtDesc(PostCategory category, Pageable pageable);
    Page<Post> findByDraftFalseAndTitleContainingIgnoreCaseOrderByPublishedAtDesc(String query, Pageable pageable);
}
