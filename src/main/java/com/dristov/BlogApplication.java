package com.dristov;

import com.dristov.domain.Account;
import com.dristov.domain.Role;
import com.dristov.repository.AccountRepository;
import com.dristov.repository.RoleRepository;
import com.dristov.service.AccountService;
import com.google.common.cache.CacheBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.guava.GuavaCacheManager;
import org.springframework.context.annotation.Bean;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@SpringBootApplication
@EnableCaching
public class BlogApplication {

	private static final Logger log = LoggerFactory.getLogger(BlogApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(BlogApplication.class, args);
	}

	@Bean
	public CacheManager cacheManager() {

		GuavaCacheManager cacheManager = new GuavaCacheManager("cache.blog");

		CacheBuilder<Object, Object> cacheBuilder = CacheBuilder.newBuilder()
				.maximumSize(100)
				.expireAfterWrite(60, TimeUnit.MINUTES);

		cacheManager.setCacheBuilder(cacheBuilder);
		return cacheManager;
	}

	@Bean
	public CommandLineRunner initDb(AccountService service) {

		return (args) -> {

			if (service.findByUsername("user") == null) {

				log.info("CREATING NEW USER...");

				Account account = new Account();
				account.setUsername("user");
				account.setPassword("password");
				account.setPasswordConfirm("password");
				account.setFullName("User");

				service.save(account);
			}
		};

	}
}