package com.dristov.web;

import com.dristov.domain.Account;
import com.dristov.domain.validator.AccountValidator;
import com.dristov.service.AccountService;
import com.dristov.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class AccountController extends BaseController {

    private AccountService accountService;
    private SecurityService securityService;
    private AccountValidator validator;

    @Autowired
    public AccountController(AccountService accountService, SecurityService securityService, AccountValidator validator) {
        this.accountService = accountService;
        this.securityService = securityService;
        this.validator = validator;
    }

    @RequestMapping(
            method = GET,
            value = "/registration"
    )
    public String register(Model model) {

        model.addAttribute("form", new Account());
        return "user_create";
    }

    @RequestMapping(
            method = POST,
            value = "/registration"
    )
    public String handleRegistration(@ModelAttribute("form") Account account, BindingResult result) {

        validator.validate(account, result);

        if (result.hasErrors()) {
            return "user_create";
        }

        accountService.save(account);
        securityService.autologin(account.getUsername(), account.getPassword());
        return "redirect:/posts";
    }

    @RequestMapping(value = "/login", method = GET)
    public String login(Model model, String error, String logout, Principal principal) {

        if (principal != null) {
            return redirect("/posts");
        }

        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");

        return "login";
    }
}
