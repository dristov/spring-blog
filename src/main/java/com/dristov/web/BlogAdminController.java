package com.dristov.web;

import com.dristov.domain.Post;
import com.dristov.form.CategoryForm;
import com.dristov.form.PostForm;
import com.dristov.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
@RequestMapping(value = "/admin")
public class BlogAdminController extends BaseController {

    private BlogService blogService;

    @Autowired
    public BlogAdminController(BlogService blogService) {
        this.blogService = blogService;
    }

    @GetMapping(value = "/posts/new")
    public String newPost(Model model) {
        model.addAttribute("postForm", new PostForm());
        model.addAttribute("categories", blogService.getAllCategories());
        model.addAttribute("create", true);
        return "admin/new";
    }

    @PostMapping(value = "/new")
    public String createPost(@Valid PostForm postForm, BindingResult bindingResult, Model model, Principal principal) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("categories", blogService.getAllCategories());
            model.addAttribute("create", true);
            return "admin/new";

        } else {

            Post post = blogService.addPost(postForm, principal.getName());

            if (post.isDraft()) {
                return redirect("/posts");

            } else {

                model.addAttribute("post", post);
                return redirect("/posts/" + post.getSlug());
            }
        }
    }

    @DeleteMapping(value = "/posts/{slug}/delete")
    public String deletePost(@PathVariable("slug") String slug) {

        Post post = blogService.getPostBySlug(slug);
        blogService.deletePost(post);

        return redirect("/posts");
    }

    // *********************************************************************************************
    // CATEGORIES
    // *********************************************************************************************

    @GetMapping(value = "/categories/new")
    public String newCategory(Model model) {
        model.addAttribute("form", new CategoryForm());
        return "admin/new_category";
    }

    @PostMapping(value = "/categories/new")
    public String createCategory(@Valid CategoryForm form, BindingResult result) {

        if (result.hasErrors()) {
            return "admin/new_category";
        }

        blogService.saveCategory(form.getName());
        return redirect("/posts/categories");
    }

    @RequestMapping(
            method = DELETE,
            value = "/comments/delete/{id}"
    )
    public String deleteComment(@PathVariable("id") Long id, @RequestParam("slug") String slug) {
        blogService.deleteComment(id);
        return redirect("/posts/" + slug);
    }
}
