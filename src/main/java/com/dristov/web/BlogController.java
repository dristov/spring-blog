package com.dristov.web;

import com.dristov.domain.Account;
import com.dristov.domain.Post;
import com.dristov.domain.exception.AuthorNotFoundException;
import com.dristov.domain.exception.PostNotFoundException;
import com.dristov.service.AccountService;
import com.dristov.service.BlogService;
import com.dristov.util.PageWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
@RequestMapping("/posts")
public class BlogController extends BaseController {

    private BlogService blogService;
    private AccountService accountService;

    @Autowired
    public BlogController(BlogService blogService, AccountService accountService) {
        this.blogService = blogService;
        this.accountService = accountService;
    }

    @RequestMapping(method = GET)
    public String index(Model model, Pageable pageable) {

        Page<Post> postPage = blogService.getPublishedPosts(pageable);
        return renderPosts(postPage, "/posts", model);
    }

    @RequestMapping(value = "/user/{username}", method = GET)
    public String showUserPosts(@PathVariable("username") String username, Model model, Pageable pageable) {

        Account account = accountService.findByUsername(username);

        if (account == null) {
            throw new AuthorNotFoundException(username);
        }

        model.addAttribute("well", "Posts by " + account.getFullName());
        return renderPosts(blogService.getPostsByAuthor(account, pageable), "/user/" + username, model);
    }

    @RequestMapping(value = "/{slug}", method = GET)
    public String showPost(@PathVariable("slug") String slug, Model model) {

        Post post = blogService.getPostBySlug(slug);

        if (post == null) {
            throw new PostNotFoundException(slug);
        }

        model.addAttribute("post", post);
        return "post";
    }

    @GetMapping(value = "/search")
    public String searchPosts(@RequestParam("q") String query, Pageable pageable, Model model) {

        if (query == null) {
            return redirect("/posts");
        }

        model.addAttribute("well", "Search: " + query);
        return renderPosts(blogService.searchPosts(query, pageable), "/posts/search?q=" + query, model);
    }

    @GetMapping(value = "/category/{category}")
    public String showPostsForCategory(@PathVariable("category") String category, Model model, Pageable pageable) {

        model.addAttribute("well", "Category: " + blogService.getCategoryName(category));
        return renderPosts(blogService.getPostsByCategory(category, pageable), "/posts/category/" + category,  model);
    }

    @GetMapping(value = "/categories")
    public String showCategories(Model model) {

        model.addAttribute("categories", blogService.getAllCategories());
        return "categories";
    }

    private String renderPosts(Page<Post> page, String url, Model model) {

        PageWrapper<Post> pageWrapper = new PageWrapper<>(page, url);

        model.addAttribute("posts", page.getContent());
        model.addAttribute("page", pageWrapper);
        return "index";
    }

    // ******************************************************************************************

    @RequestMapping(
            method = POST,
            value = "/{slug}/comment/add"
    )
    public String addComment(@PathVariable("slug") String postSlug, String comment, Principal principal) {

        if (comment == null) {
            logI("comment form is null");
            blogService.addComment("this is a comment", principal.getName(), postSlug);
        } else {
            blogService.addComment(comment, principal.getName(), postSlug);
        }

        return redirect("/posts/" + postSlug);
    }
}
