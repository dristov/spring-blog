package com.dristov.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BaseController {

    private static final Logger logger = LoggerFactory.getLogger("ControllerLogger");

    protected String redirect(String path) {
        return "redirect:" + path;
    }

    protected void logI(String str) {
        logger.info(str);
    }


}
