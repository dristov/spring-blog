insert into roles(name) values('ADMIN');
insert into roles(name) values('MOD');
insert into roles(name) values('USER');

insert into categories(display_name, url_slug) values('Spring', 'spring');
insert into categories(display_name, url_slug) values('Android', 'android');